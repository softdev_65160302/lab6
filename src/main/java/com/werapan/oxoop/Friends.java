/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.oxoop;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author informatics
 */
public class Friends implements Serializable{

    private int id;
    private String name;
    private int age;
    private String tel;
    private static int lastId = 1;

    public Friends(String name, int age, String tel) {
        this.id = lastId++;
        this.name = name;
        this.age = age;
        this.tel = tel;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getTel() {
        return tel;
    }

    public static int getLastId() {
        return lastId;
    }

    public void setId(int id) {
        
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) throws Exception {
        if (age < 0) {
            throw new Exception();
        }
        this.age = age;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public static void main(String[] args) {
        try {
            Friends f = new Friends("somsuk", 111, "0873332221");
            f.setAge(-1);
            System.out.println(""+f);
        } catch (Exception ex) {
            System.out.println("Age is lowwer than 0");
        }
    }

    @Override
    public String toString() {
        return "Friends{" + "id=" + id + ", name=" + name + ", age=" + age + ", tel=" + tel + '}';
    }
    
    
}
